<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" <?php language_attributes(); ?> "> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?> > <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title(''); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
	<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">
	<?php wp_head(); ?>
	<?php include_once("tracking.php") ?>
</head>

<body <?php body_class('antialiased'); ?>>
<?php include_once("analytics.php") ?>
<nav>
	 <div class="overlay overlay-slidedown right">
		 <div class="row">
			<ul>
			    <li class="block">
			    	<input type="radio" name="mobile" id="furniture" />
			    	<label for="furniture">Furniture</label>
			    	<ul class="info">
						<li><a href="<?php echo get_bloginfo('url'); ?>/living" title="Living Room">Living</a></li>
						<li><a href="<?php echo get_bloginfo('url'); ?>/dining" title="Dining Room">Dining</a></li>
						<li><a href="<?php echo get_bloginfo('url'); ?>/bedroom" title="Bedroom">Bedroom</a></li>
						<li><a href="<?php echo get_bloginfo('url'); ?>/office" title="Office">Office</a></li>
					</ul>
			    </li>
				<li class="block">
					<input type="radio" name="mobile" id="craftsmanship" />
					<label for="craftsmanship">Craftsmanship</label>
					<ul class="info">
						<li><a href="<?php echo get_bloginfo('url'); ?>/story" title="Our Story">Our Story</a></li>
						<li><a href="<?php echo get_bloginfo('url'); ?>/carpenters" title="Our People">Our People</a></li>
						<li><a href="<?php echo get_bloginfo('url'); ?>/our-promise/" title="Our Guarantee">Our Guarantee</a></li>
						<li><a href="<?php echo get_bloginfo('url'); ?>/custom" title="">Your Customization</a></li>
					</ul>
				</li>
				<li class="block">
					<input type="radio" name="mobile" id="connect" />
					<label for="connect">Connect</label>
					<ul class="info">
						<li><a href="<?php echo get_bloginfo('url'); ?>/showroom/" title="Showroom">Showroom</a></li>
						<li><a href="<?php echo get_bloginfo('url'); ?>/directions" title="Directions">Directions</a></li>
						<li><a href="<?php echo get_bloginfo('url'); ?>/contact" title="Contact Us">Contact Us</a></li>
						<li><a href="<?php echo get_bloginfo('url'); ?>/faqs" title="Frequenty Asked Questions">FAQ</a></li>
						<li><a href="<?php echo get_bloginfo('url'); ?>/reviews" title="Honest Reviews">Honest Reviews</a></li>
						<li><a href="<?php echo get_bloginfo('url'); ?>/delivery/" title="">Get it Delivered</a></li>
						<li><a href="<?php echo get_bloginfo('url'); ?>/table-stories/" title="">Table Stories</a></li>
					</ul>
				</li>
			</ul>
		 </div>
	 </div>
</nav>
<div class="overlay-appointment overlay-scale">
	<div class="row">
		<div class="right">
			<a class="overlay-close">×</a>
		</div>
		<div class="form-container large-12 columns">
	        <br><br><p class="text-center" style="font-size:42px;line-height:48px;"><strong>Two Easy Ways to Visit Us</strong></p><br>
			<p class="text-center" style="font-size:28px;line-height:30px;">— Weekly Walk-In Tour —</br>
			<span style="font-size:22px">If you are interested in a tour, but aren’t quite ready to talk about a furniture purchase then we invite you to come any Wednesday at 4:00pm for our <em><a href="/tour" style="color: #43382A;text-decoration:none">Weekly Walk-In Tour</a></em>.</span></p>
			<p class="text-center" style="font-size:28px;line-height:30px;">— Personal Consultation —</br>
			<span style="font-size:22px">If you are interested in furniture then we ask you to schedule a personal visit through the calendar below. A personal consultation ensures you get the time and attention you need to talk through the intricacies of your space.</span></p>
			<iframe src="https://unruhfurniture.youcanbook.me/?noframe=true&skipHeaderFooter=true" id="ycbmiframeunruhfurniture" style="width:100%;height:700px;border:0px;background-color:transparent;" frameborder="0" allowtransparency="true"></iframe>
			<script>window.addEventListener && window.addEventListener("message", function(event){if (event.origin === "https://unruhfurniture.youcanbook.me"){document.getElementById("ycbmiframeunruhfurniture").style.height = event.data + "px";}}, false);</script>
		</div>
	</div>
</div>
<header class="contain-to-grid">
	<!-- Starting the Top-Bar -->
	<div class="fixed">
		<div class="very-top-bar-wrap top">
			<div class="row">
				<div class="cart-bar">
					<div class="row">
						<div class="large-12 columns">
							<div class="right">
								<span class="favs" style="margin-right:15px;"><span style="color:#beb089;">Call | Text</span> <a href="tel:8168131066" style="color:white;">(816) 813-1066</a></span><a class="favs" href="<?php echo get_bloginfo('url'); ?>/deposit/"><span class="text"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/_icons/wallet.png" width="25"/> Make Deposit</span></a><a class="cart" href="<?php echo get_bloginfo('url'); ?>/cart/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/_icons/cart.png" width="25"/> <span class="text">My Cart</span></a><a class="cart-count" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php global $woocommerce; $count = $woocommerce->cart->cart_contents_count; if ($count > 0) { echo ''; echo $count; echo '';} else {echo "0";}?></a>
							</div>
						</div>
						<div id="loginModal" class="reveal-modal" data-reveal aria-labelledby="loginModal" aria-hidden="true" role="dialog">
							<?php wp_login_form(); ?>
							<a class="close-reveal-modal" aria-label="Close">×</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<nav class="top-bar dropdown-animation" data-topbar>
		    <ul class="title-area">
		        <li class="name">
		        	<h1><a href="<?php echo get_site_url(); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" /></a></h1>
		        </li>
				<li>
					<div class="right-small">
						<a role="button" id="toggle" class="hamburger" ><span></span></a>
					</div>
				</li>
		    </ul>
		    <section class="top-bar-section">
		    <ul class="right">
	        <li class="has-dropdown megamenu"><a class="dropdown-link furniture" href="#">Furniture</a>
	        	<ul class="dropdown dropdown-wrapper">
		            <li>
	              <div class="dropdown-content-wrap furniture">
	             	<a class="nav-block-link" href="<?php echo get_bloginfo('url'); ?>/living" title="Living Room">
	                <div class="large-3 columns nav-block">
	                    <img src="<?php $options = get_option('sample_theme_options'); echo $options['living-image']; ?>" width="300">
	                    <h3>Living</h3>
	                </div>
	                </a>
	                <a class="nav-block-link" href="<?php echo get_bloginfo('url'); ?>/dining" title="Dining Room">
	                <div class="large-3 columns nav-block">
	                    <img src="<?php $options = get_option('sample_theme_options'); echo $options['dining-image']; ?>" width="300">
	                    <h3>Dining</h3>
	                </div>
	                </a>
	                <a class="nav-block-link" href="<?php echo get_bloginfo('url'); ?>/bedroom" title="Bedroom">
	                <div class="large-3 columns nav-block">
	                	<img src="<?php $options = get_option('sample_theme_options'); echo $options['bed-image']; ?>" width="300">
	                    <h3>Bedroom</h3>
	                </div>
	                </a>
	                <a class="nav-block-link" href="<?php echo get_bloginfo('url'); ?>/office" title="Office">
		            <div class="large-3 columns nav-block">
		            	<img src="<?php $options = get_option('sample_theme_options'); echo $options['office-image']; ?>" width="300">
		                <h3>Office</h3>
		            </div>
	              	</a>
	                <div class="clearfix"></div>
	              </div>
	            </li>
	          </ul>
	        </li>
	        <li class="has-dropdown megamenu"><a class="dropdown-link craftsmanship" href="#">Craftsmanship</a>
	          <ul class="dropdown dropdown-wrapper">
	            <li>
	              <div class="dropdown-content-wrap craftsmanship">
	             	<a class="nav-block-link" href="<?php echo get_bloginfo('url'); ?>/story" title="Our Story">
	                <div class="large-3 columns nav-block">
	                    <img src="<?php $options = get_option('sample_theme_options'); echo $options['ourstory-image']; ?>" width="300">
	                    <h3>Our Story</h3>
	                </div>
	                </a>
	                <a class="nav-block-link" href="<?php echo get_bloginfo('url'); ?>/carpenters" title="The Guys">
	                <div class="large-3 columns nav-block">
	                    <img src="<?php $options = get_option('sample_theme_options'); echo $options['theguys-image']; ?>" width="300">
	                    <h3>Our People</h3>
	                </div>
	                </a>
	                <a class="nav-block-link" href="<?php echo get_bloginfo('url'); ?>/our-promise/" title="Our Promise">
	                <div class="large-3 columns nav-block">
	                	<img src="<?php $options = get_option('sample_theme_options'); echo $options['quality-image']; ?>" width="300">
	                    <h3>Our Guarantee</h3>
	                </div>
	                </a>
	                <a class="nav-block-link" href="<?php echo get_bloginfo('url'); ?>/custom" title="Honest Reviews">
		            <div class="large-3 columns nav-block">
		            	<img src="<?php $options = get_option('sample_theme_options'); echo $options['reviews-image']; ?>" width="300">
		                <h3>Your Customization</h3>
		            </div>
	              	</a>
	                <div class="clearfix"></div>
	              </div>
	            </li>
	          </ul>
	        </li>
	        <li class="has-dropdown megamenu"><a class="dropdown-link connect" href="#">Connect</a>
	          <ul class="dropdown dropdown-wrapper">
	            <li>
	              <div class="dropdown-content-wrap connect">
	             		<div class="large-3 columns bottom nav-block" style="background:#cab598 !important;">
		                    <h3 style="font-size:25px;color:white;margin-top:10px;" class="title">Welcome to the<br/>Community</h3>
		                </div>
		                <a class="nav-block-link" href="<?php echo get_bloginfo('url'); ?>/showroom/" title="Showroom">
			                <div class="large-3 columns bottom nav-block">
			                    <h3>Showroom</h3>
			                    <p>Furniture in a Church is worth the visit</p>
			                </div>
		                </a>
	              		<a class="nav-block-link" href="<?php echo get_bloginfo('url'); ?>/directions" title="Directions">
			                <div class="large-3 columns nav-block">
			                    <h3>Directions</h3>
			                    <p>Where to find the Gentlemen Carpenters</p>
			                </div>
		                </a>
		                <a class="nav-block-link" href="<?php echo get_bloginfo('url'); ?>/contact" title="Contact Us">
			                <div class="large-3 columns bottom nav-block">
			                    <h3>Contact Us</h3>
			                    <p>We would love to hear from you today</p>
			                </div>
	              		</a>
		                <a class="nav-block-link" href="<?php echo get_bloginfo('url'); ?>/faqs" title="Questions">
			                <div class="large-3 columns nav-block">
			                    <h3>FAQs</h3>
			                    <p>Easy answers to common questions</p>
			                </div>
		                </a>
		                <a class="nav-block-link" href="<?php echo get_bloginfo('url'); ?>/reviews" title="Honest Reviews">
			                <div class="large-3 columns bottom nav-block">
			                    <h3>Honest Reviews</h3>
			                    <p>Real testimonials from real people.</p>
			                </div>
		                </a>
		                <a class="nav-block-link" href="<?php echo get_bloginfo('url'); ?>/delivery/" title="Get it Delivered">
			                <div class="large-3 columns bottom nav-block">
			                    <h3>Get it Delivered</h3>
			                    <p>See how an Unruh Delivery saves the day</p>
			                </div>
		                </a>
		                <a class="nav-block-link" href="<?php echo get_bloginfo('url'); ?>/table-stories/" title="Table Stories">
			                <div class="large-3 columns bottom nav-block">
			                    <h3>Table Stories</h3>
			                    <p>Candid stories from around the table</p>
			                </div>
		                </a>
	                <div class="clearfix"></div>
	              </div>
	            </li>
	          </ul>
	        </li>
	        <li class="appointment appointment-shadow">
				<a title="Appointment" id="toggle-appointment-top">Schedule an<br/>Appointment</a>
				<div class="appointment-bottom">
					<p class="right"><?php $options = get_option('sample_theme_options'); echo $options['appointment-offer']; ?></p>
					<a role="button" id="toggle-appointment">Don't keep Pete waiting ></a>
				</div>
			</li>
	      </ul>
		    </section>
		</nav>
		<div class="very-top-bar-wrap bottom">
			<div class="very-top-bar row">
				<div class="small-7 columns appointment-bar text-center">
					<a href="#" role="button" id="toggle-appointment-mobile" title="Appointment">Schedule an Appointment</a>
				</div>
				<div class="small-5 columns cart-bar">
					<div class="row">
						<div class="large-12 columns">
							<div class="right">
								<a href="<?php echo get_bloginfo('url'); ?>/deposit/" style="margin-right:15px;"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/_icons/wallet.png" width="25" /></a><a style="margin-right:5px;" href="<?php echo get_bloginfo('url'); ?>/cart/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/_icons/cart.png" width="25" /></a><a class="cart-count" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php global $woocommerce; $count = $woocommerce->cart->cart_contents_count; if ($count > 0) { echo ''; echo $count; echo '';}else {echo "0";}?></a>
							</div>
						</div>
						<div id="loginModal" class="reveal-modal" data-reveal aria-labelledby="loginModal" aria-hidden="true" role="dialog">
							<?php wp_login_form(); ?>
							<a class="close-reveal-modal" aria-label="Close">×</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End of Top-Bar -->
</header>
<?php if (is_product()) { ?>
<div class="video-overlay">
  <a class="close-video-overlay">×</a>
  <div class="video-container">
	  <div class="flex-video widescreen vimeo">
	    <iframe id="product-video" src="<?php echo get_post_meta( $post->ID, '_text_field', true ); ?>?api=1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	  </div>
  </div>
</div>
<?php } ?>
<!-- Start the main container -->
<div class="container" role="document">
