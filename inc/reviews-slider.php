<?php if( have_rows('featured_reviews') ): ?>
 
    <div class="flex-slider">
        <div class="feature">
            <div class="slickslide">
 
                <?php while( have_rows('featured_reviews') ): the_row(); 

            	// override $post
            	$post = get_sub_field('review');
            	setup_postdata( $post ); 
                $product_image = get_field('additional_image'); 
                ?>
                
                <div data-headshot="<?php the_post_thumbnail_url('large'); ?>" style="background-image:url(<?php echo $product_image['url']; ?>);">                
                    <div class="review-text">
                        <div class="review-content">
                            <p class="review-quote"><?php the_content(); ?></p>
                            <p class="review-meta"><?php the_title(); ?>, <?php the_field('year'); ?></p>
                        </div>
                    </div>
                </div>
                 
                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

                <?php endwhile; ?>
     
            </div>
        </div>
    </div>

<?php endif; ?>