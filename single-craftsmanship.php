<?php get_header(); ?>

	<?php $title = get_post_meta(get_the_ID(), 'title', true); ?>
	<?php $videourl = get_post_meta(get_the_ID(), 'videourl', true); ?>
	<?php $subtitle = get_post_meta(get_the_ID(), 'subtitle', true); ?>
	<?php global $post; $slug = get_post( $post )->post_name; ?>
	<?php while (have_posts()) : the_post(); ?>
	<div class="video-overlay">
			      <a class="close-video-overlay">&#215;</a>
			      <div class="video-container">
					  <div class="flex-video widescreen vimeo">
					    <iframe id="<?php echo $slug ?>" src="<?php echo $videourl?>?api=1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					  </div>
				  </div>
				</div>
	<div class="small-12 large-12 columns" id="content" role="main">
		<div class="craftsmanship-top" data-parallax="scroll" data-image-src="<?php echo $image = wp_get_attachment_url( get_post_thumbnail_id($post->ID)); ?>" data-natural-width="1600" data-natural-height="1148">
			<div class="row">
			<div class="large-12 columns text-center">
				<h1 class="white"><?php echo $title?></h1>
				<a class="start-video"><img class="play-button craftsmanship-bottom" src="<?php echo $upload_dir['baseurl']; ?>/2016/01/play-button.png" alt="play-button" class="alignnone"/></a>
			</div>
			</div>
			<div style="background:rgba(94, 83, 69, 0.8);padding:40px 0;" class="text-center">
				<img class="crest" src="<?php echo $upload_dir['baseurl']; ?>/2016/04/crest-edit.png" alt="crest"/><h2 style="display:inline-block;"><?php echo $subtitle?> <span style="white-space:nowrap;">Be Unruhly</span>.</h2>
			</div>
		</div>
			<?php the_content(); ?>
	</div>
	<?php endwhile;?>

	<script>
	  $(document).ready(function(){
		  // Iframe/player variables
		  var iframe = $('#<?php echo $slug ?>')[0];
		  var player = $f(iframe);
		  // Open on play
		  $('.start-video').click(function(){
		    $('.video-overlay').css('left', 0)
		    $('.video-overlay').addClass('show')
		    player.api("play");
		  })
		  // Closes on click outside
		  $('.close-video-overlay').click(function(){
		    $('.video-overlay').removeClass('show')
		    setTimeout(function() {
		      $('.video-overlay').css('left', '-100%')
		    }, 300);
		    player.api("pause");
		  })
		});
  </script>

<?php get_footer(); ?>