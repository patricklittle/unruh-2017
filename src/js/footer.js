// ==== FOOTER ==== //

// A simple wrapper for all your custom jQuery that belongs in the footer
;
(function($) {
    $(function() {
        // Example integration: JavaScript-based human-readable timestamps
        // if ($.timeago) {
        //   $('time').timeago();
        // }
 
        $(".slickslide").slick({
            dots: true,
            infinite: true,
            speed: 800,
            fade: true,
            cssEase: "linear",
            slidesToShow: 1,
            autoplay: true,
            arrows: false,
            prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><svg width="20" height="20" viewBox="0 0 12 20" xmlns="http://www.w3.org/2000/svg"><path d="M11 .1L1.1 10l9.9 9.9" stroke="#FFF" fill="none" fill-rule="evenodd"/></svg></button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><svg width="20" height="20" viewBox="0 0 12 20" xmlns="http://www.w3.org/2000/svg"><path d="M1 .1l9.9 9.9L1 19.9" stroke="#FFF" fill="none" fill-rule="evenodd"/></svg></button>',
            customPaging: function(slider, i) {
                var headshot = $(slider.$slides[i]).data("headshot");
                return '<a style="background-image: url(' + headshot + ');"></a>';
            },
            responsive: [{ 
                breakpoint: 768,
                settings: {
                    dots: false,
                    arrows: true,
                    autoplay: false,
                } 
            }]
        });
    });
}(jQuery));