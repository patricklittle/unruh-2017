<?php
/*
Template Name: Home
*/
get_header(); ?>

	<div class="small-12 large-12 columns" id="content" role="main">

		<a class="anchor" name="home"></a>
		<?php $page = get_posts( array( 'post_type' => 'page', 'name' => 'home' ) ); ?>
		<?php echo $page[0]->post_content; ?>

	</div>

<?php get_footer(); ?>
