<?php
/*********************
Enqueue the proper CSS
if you use Sass.
*********************/
if( ! function_exists( 'reverie_enqueue_style' ) ) {
	function reverie_enqueue_style()
	{
    	// foundation stylesheet
    	wp_register_style( 'reverie-foundation-stylesheet', get_stylesheet_directory_uri() . '/_css/app.css', array(), '' );
    	
        // Register the main style
    	wp_register_style( 'reverie-stylesheet', get_stylesheet_directory_uri() . '/_css/style.css', array(), '', 'all' );

        // Unruh Furniture Stylesheet    
        wp_register_style( 'unruhfurniture-style', get_stylesheet_uri(), $dependencies = array(), filemtime( get_template_directory() . '/style.css' ) );

		global $ver_num; // define global variable for the version number
	  	$ver_num = mt_rand(); // on each call/load of the style the $ver_num will get different value
		
		wp_enqueue_style( 'unruhfurniture-style', array(), $ver_num, 'all' );

		// wp_enqueue_style( 'reverie-foundation-stylesheet' ); // old style
		// wp_enqueue_style( 'reverie-stylesheet' ); // old Stylesheet
	}

}
add_action( 'wp_enqueue_scripts', 'reverie_enqueue_style' );

