<?php
// http://codex.wordpress.org/Function_Reference/register_post_type
function register_reviews() {
	
    // --------------------------------
	// Reviews
	// --------------------------------
	$labels = array(
		'name' => _x('Reviews', 'post type general name'),
		'singular_name' => _x('Review', 'post type singular name'),
		'add_new' => _x('Add New', 'review'),
		'add_new_item' => __('Add New Review'),
		'edit_item' => __('Edit Review'),
		'new_item' => __('New Review'),
		'view_item' => __('View Review'),
		'search_items' => __('Search Reviews'),
		'not_found' =>  __('No Reviews found'),
		'not_found_in_trash' => __('No Reviews found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => 'Reviews'
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => false,
		'query_var' => true,
		'rewrite' => array('slug' => 'review'),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => 29,
		'menu_icon' => 'dashicons-filter',
		'supports' => array('title', 'page-attributes', 'thumbnail', 'editor')
	);
	register_post_type('review', $args);

}
add_action( 'init', 'register_reviews' );
