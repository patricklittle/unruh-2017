
var gulp         = require('gulp');
var watch        = require('gulp-watch');
var sass         = require('gulp-sass');
var browserSync  = require('browser-sync');
var prefix       = require('gulp-autoprefixer');
var plumber      = require('gulp-plumber');
var uglify       = require('gulp-uglify');
var rename       = require('gulp-rename');
var imagemin     = require('gulp-imagemin');
var pngquant     = require('imagemin-pngquant');

/**
*
* Styles
* - Compile
* - Compress/Minify
* - Catch errors (gulp-plumber)
* - Autoprefixer
*
**/
gulp.task('sass', function() {
  gulp.src('src/scss/style.scss')
  // .pipe(watch('src/sass/*.scss'))
  .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(prefix('last 2 versions', '> 1%', 'ie 8', 'Android 2', 'Firefox ESR'))
  .pipe(plumber())
  .pipe(gulp.dest(''));
});

// Fonts
// We're just moving them for now
gulp.task('fonts', function() {
  gulp.src('src/fonts/**/*')
  .pipe(plumber())
  .pipe(gulp.dest('assets/fonts'));
});


/**
*
* Javascript
* - Uglify
*
**/
gulp.task('scripts', function() {
  gulp.src('src/js/*.js')
  // .pipe(watch('src/js/*.js'))
  .pipe(uglify())
  .pipe(rename({
    suffix: ".min",
  }))
  .pipe(gulp.dest('assets/js'))
});

/**
*
* Images
* - Compressor
*
**/
gulp.task('images', function () {
  return gulp.src('src/img/**/*')
  // .pipe(watch('src/images/*'))
  .pipe(imagemin({
    progressive: true,
    svgoPlugins: [{removeViewBox: false}],
    use: [pngquant()]
  }))
  .pipe(gulp.dest('assets/img'));
});


/**
*
* Default task
* - Runs sass, browser-sync, scripts and image tasks
* - Watchs for file changes for images, scripts and sass/css
*
**/
gulp.task('default', ['sass', 'scripts', 'images', 'fonts'], function () {
  gulp.watch('src/scss/**/*.scss', ['sass']);
  gulp.watch('src/js/**/*.js', ['scripts']);
  gulp.watch('src/img/*', ['images']);
});
