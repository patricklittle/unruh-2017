<?php get_header(); ?>

	<div class="small-12 large-12 columns" id="content" role="main">
		<div class="padding-medium" data-parallax="scroll" data-image-src="<?php echo $upload_dir['baseurl']; ?>/2016/01/faq-bg.jpg" data-natural-width="1600" data-natural-height="527">
		<div class="row">
			<div class="large-12 columns">
				<h1 class="white">Error 404</h1>
			</div>
		</div>
		</div>
		<div style="background-color:#ffffff;padding:150px 0;text-align: center;">
			<div class="row">
				<h1>The page you are looking for isn't here.<br/>Pete thinks you should <a href="<?php echo get_site_url(); ?>" style="color:#C8B499;">try again</a>.</h1>
			</div>
		</div>
	</div>

<?php get_footer(); ?>