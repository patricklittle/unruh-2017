<?php
/*
Template Name: Reviews
*/
get_header(); ?>

	<div class="small-12 large-12 columns landing-page" id="content" role="main">
	
		<?php while (have_posts()) : the_post(); ?>
			
			<?php if(get_field('video_header_mobile')) : ?>
				<div class="show-for-small-only">
					<?php the_field('video_header_mobile'); ?>
				</div>
				
				<div class="show-for-medium-up">
					<?php if(get_field('video_header_desktop')) { the_field('video_header_desktop'); } ?>
				</div>				
			<?php else : ?>
				<?php if(get_field('video_header_desktop')) { the_field('video_header_desktop'); } ?>
			<?php endif; ?>
			
			
			<div class="landing-page-banner">
				<img class="crest" src="<?php echo get_template_directory_uri(); ?>/assets/img/crest.png" alt="crest"/>
				<h2>Love your work and you will find joy in its mastery.<span>Be Unruhly</span>.</h2>
			</div>

			<div class="padding-small">
				<div class="row landing-page-content">
					<?php the_content(); ?>
				</div>
			</div>
			
		<?php endwhile;?>

		<div class="full-width-block clearfix reviews-grid" data-masonry='{ "itemSelector": ".review-message" }'>

		<?php $loop = new WP_Query( array( 'post_type' => 'review', 'posts_per_page' => -1 ) ); ?>
		
		<?php $reviewCount = 0; ?>
		<?php while ( $loop->have_posts() ) : $loop->the_post(); 
		
		$reviewCount++;
		$location = get_field('review_location');
		$year = get_field('year');
		$review_id = $post->ID;
		
		?>

	        <div class="medium-6 large-4 columns review-message">
	            <div class="review-stars"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/five-stars.svg" alt="Furniture Reviews"/></div>
	            <blockquote>
	                <?php the_content(); ?>
	                <cite><?php the_title(); ?>, <?php the_date('F Y'); ?></cite>
	            </blockquote>
				<?php if ( has_post_thumbnail() ) : ?>
		            <div class="review-portrait-wrap">
		                <?php the_post_thumbnail('small-square'); ?>
		            </div>
				<?php endif; ?>
	        </div>

			<?php //if($reviewCount == 4):
				
				// $reviewCount == 0; 
	
				// <div class="medium-6 large-4 columns review-cta">
				// 	<div class="review-cta-wrap">
				// 		<h4>Come see for yourself why we receive so many five star reviews.</h4>
				// 		<p><a title="Appointment" id="toggle-appointment-top" class="button">Schedule an Appointment today</a></p>	
				// 	</div>
				// </div>
			
				// endif; ?>
			
		<?php endwhile; wp_reset_query(); ?>
	</div>
    
	<script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/4.2.0/masonry.pkgd.min.js"></script>
	
<?php get_footer(); ?>
