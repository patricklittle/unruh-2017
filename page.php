<?php get_header(); ?>

	<div class="small-12 large-12 columns" id="content" role="main">
		<?php while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile;?>
	</div>

<?php get_footer(); ?>