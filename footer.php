</div><!-- Container End -->
<div class="clearfix"></div>

<footer class="full-width" role="contentinfo">
	<div class="row">
		<div class="medium-12 text-center columns">
			<div class="left">
				<p>&#169; unruh furniture</p>
			</div>
			<div style="display:inline-block;">
				<a href="https://www.facebook.com/UnruhFurniture/" target="_blank" class="facebook"></a>
				<a href="https://www.instagram.com/unruhfurniture/" target="_blank" class="instagram"></a>
				<a href="https://twitter.com/unruhfurniture" target="_blank" class="twitter"></a>
			</div>
			<div class="right">
				<p><a href="<?php echo site_url(); ?>/work">Join Our Team!</a></p>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

<script>
	(function($) {
		$(document).foundation();
	})(jQuery);
	$(".megamenu").mouseenter(function(){
	  if($(window).width()>641){
	    $(".dropdown-wrapper").offset({left: 0});
	    $(".dropdown-wrapper").css("width",$(window).width());
	  }else{
	    $(".dropdown-wrapper").css("width",auto);
	  }
	});
</script>

<!-- START Google Code for Remarketing Tag -->
<div style="display:none;">
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 949746193;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/949746193/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</div>
<!-- END Google Code for Remarketing Tag -->

<!-- START Twitter/Pinterest Tracking Tag -->
<?php if (is_page('home')) { ?>
<img height="1" width="1" style="display:none;" alt="" src="https://ct.pinterest.com/?tid=jg9gIaq8cgB&value=0.00&quantity=1"/>
<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">twttr.conversion.trackPid('nu6y8', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=nu6y8&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=nu6y8&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
</noscript>
<?php } ?>
<?php if (is_page('checkout')) { ?>
<img height="1" width="1" style="display:none;" alt="" src="https://ct.pinterest.com/?tid=53aSkA5Svrt&value=0.00&quantity=1"/>
<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">twttr.conversion.trackPid('nu6xq', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=nu6xq&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=nu6xq&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
</noscript>
<?php } ?>
<?php if (is_page('thank-you-showroom')) { ?>
<img height="1" width="1" style="display:none;" alt="" src="https://ct.pinterest.com/?tid=ryd7zVxLtVZ&value=0.00&quantity=1"/>
<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">twttr.conversion.trackPid('nu6ya', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=nu6ya&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=nu6ya&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
</noscript>
<?php } ?>
<?php if (is_page('thank-you-purchase')) { ?>
<img height="1" width="1" style="display:none;" alt="" src="https://ct.pinterest.com/?tid=3IrJZkJ2tSn&value=0.00&quantity=1"/>
<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">twttr.conversion.trackPid('nu6y9', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=nu6y9&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=nu6y9&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
</noscript>
<?php } ?>
<?php if (is_page('showroom-offer-1')) { ?>
<img height="1" width="1" style="display:none;" alt="" src="https://ct.pinterest.com/?tid=4TQMitcyjBU&value=0.00&quantity=1"/>
<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">twttr.conversion.trackPid('nu6yb', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=nu6yb&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=nu6yb&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
</noscript>
<?php } ?>
<?php if (is_page('showroom-offer-2')) { ?>
<img height="1" width="1" style="display:none;" alt="" src="https://ct.pinterest.com/?tid=aDjmEDnOJQk&value=0.00&quantity=1"/>
<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">twttr.conversion.trackPid('nu6yc', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=nu6yc&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=nu6yc&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
</noscript>
<?php } ?>
<?php if (is_page('showroom-offer-3')) { ?>
<img height="1" width="1" style="display:none;" alt="" src="https://ct.pinterest.com/?tid=gSbJsJQS97I&value=0.00&quantity=1"/>
<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">twttr.conversion.trackPid('nu6ye', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=nu6ye&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=nu6ye&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
</noscript>
<?php } ?>
<!-- END Twitter/Pinterest Tracking Tag -->

</body>
</html>