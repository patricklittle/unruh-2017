<?php
/*
Author: Zhen Huang
URL: http://themefortress.com/

This place is much cleaner. Put your theme specific codes here,
anything else you may want to use plugins to keep things tidy.

*/

/*
1. lib/clean.php
  - head cleanup
	- post and images related cleaning
*/
require_once('_lib/clean.php'); // do all the cleaning and enqueue here

/*
2. lib/enqueue-style.php
    - enqueue Foundation and Reverie CSS
*/
require_once('_lib/enqueue-style.php');

/*
3. lib/foundation.php
	- add pagination
*/
require_once('_lib/foundation.php'); // load Foundation specific functions like top-bar
/*
4. lib/nav.php
	- custom walker for top-bar and related
*/
require_once('_lib/nav.php'); // filter default wordpress menu classes and clean wp_nav_menu markup
/*
5. lib/presstrends.php
    - add PressTrends, tracks how many people are using Reverie
*/
require_once('_lib/presstrends.php'); // load PressTrends to track the usage of Reverie across the web, comment this line if you don't want to be tracked

require_once('_lib/reviews.php'); // Custom Post Type for Reviews

// Load theme options
require_once ( get_stylesheet_directory() . '/theme-options.php' );

/**********************
Add theme supports
 **********************/
if( ! function_exists( 'reverie_theme_support' ) ) {
    function reverie_theme_support() {
        // Add language supports.
        load_theme_textdomain('reverie', get_template_directory() . '/_lang');

        // Add post thumbnail supports. http://codex.wordpress.org/Post_Thumbnails
        add_theme_support('post-thumbnails');
        // set_post_thumbnail_size(150, 150, false);
        add_image_size('fd-lrg', 1024, 99999);
        add_image_size('fd-med', 768, 99999);
        add_image_size('fd-sm', 320, 9999);
        add_image_size('small-square', 200, 200, true );

        // rss thingy
        add_theme_support('automatic-feed-links');

        // Add post formats support. http://codex.wordpress.org/Post_Formats
        add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));

        // Add menu support. http://codex.wordpress.org/Function_Reference/register_nav_menus
        add_theme_support('menus');
        register_nav_menus(array(
            'primary' => __('Primary Navigation', 'reverie'),
            'secondary' => __('Secondary Navigation', 'reverie')
        ));

        // Add custom background support
        add_theme_support( 'custom-background',
            array(
                'default-image' => '',  // background image default
                'default-color' => '', // background color default (dont add the #)
                'wp-head-callback' => '_custom_background_cb',
                'admin-head-callback' => '',
                'admin-preview-callback' => ''
            )
        );
    }
}
add_action('after_setup_theme', 'reverie_theme_support'); /* end Reverie theme support */

remove_filter( 'the_content', 'wpautop' );

// create widget areas: sidebar, footer
$sidebars = array('Sidebar');
foreach ($sidebars as $sidebar) {
    register_sidebar(array('name'=> $sidebar,
    	'id' => 'Sidebar',
        'before_widget' => '<article id="%1$s" class="panel widget %2$s">',
        'after_widget' => '</article>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));
}
$sidebars = array('Footer');
foreach ($sidebars as $sidebar) {
    register_sidebar(array('name'=> $sidebar,
    	'id' => 'Footer',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));
}
$sidebars = array('CTA Boxes');
foreach ($sidebars as $sidebar) {
    register_sidebar(array('name'=> $sidebar,
    	'id' => 'cta-Boxes',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));
}

// return entry meta information for posts, used by multiple loops, you can override this function by defining them first in your child theme's functions.php file
if ( ! function_exists( 'reverie_entry_meta' ) ) {
    function reverie_entry_meta() {
        echo '<span class="byline author">'. __('Written by', 'reverie') .' <a href="'. get_author_posts_url(get_the_author_meta('ID')) .'" rel="author" class="fn">'. get_the_author() .', </a></span>';
        echo '<time class="updated" datetime="'. get_the_time('c') .'" pubdate>'. get_the_time('F jS, Y') .'</time>';
    }
};

// show/hide wp admin bar
add_filter('show_admin_bar', '__return_false');

// hide unessecary wp admin menu pages
add_action( 'admin_menu', 'my_remove_menu_pages' );
function my_remove_menu_pages() {
	remove_menu_page('edit-comments.php');
}

// enable excerpt
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

// add body class to pages
function add_slug_body_class( $classes ) {
global $post;
if ( isset( $post ) ) {
$classes[] = $post->post_type . '-' . $post->post_name;
}
return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

//post types
add_action( 'init', 'create_craftsmanship' );

function create_craftsmanship() {
    register_post_type( 'craftsmanship',
        array(
            'labels' => array(
                'name' => 'Craftsmanship',
                'singular_name' => 'Craftsmanship',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Craftsmanship',
                'edit' => 'Edit',
                'edit_item' => 'Edit Craftsmanship',
                'new_item' => 'New Craftsmanship',
                'view' => 'View',
                'view_item' => 'View Craftsmanship',
                'search_items' => 'Search Craftsmanship',
                'not_found' => 'No Craftsmanship found',
                'not_found_in_trash' => 'No Craftsmanship found in Trash',
                'parent' => 'Parent Craftsmanship'
            ),

            'public' => true,
            'menu_position' => 50,
            'supports' => array( 'title', 'editor', 'comments', 'thumbnail', 'custom-fields', 'excerpt', 'tags' ),
            'taxonomies' => array( 'category', 'post_tag' ),
            'menu_icon'=> 'dashicons-admin-tools',
            'has_archive' => true
        )
    );
}

add_action('init', 'craftsmanship_add_default_boxes');

function craftsmanship_add_default_boxes() {
    register_taxonomy_for_object_type('category', 'craftsmanship');
    register_taxonomy_for_object_type('post_tag', 'craftsmanship');
}

$upload_dir = wp_upload_dir();

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_filter( 'woocommerce_product_tabs', 'wcs_woo_remove_reviews_tab', 98 );
function wcs_woo_remove_reviews_tab($tabs) {
 unset($tabs['reviews']);
 return $tabs;
}
// Display 24 products per page. Goes in functions.php
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 100;' ), 20 );

// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	ob_start();
	?>
	<a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php echo sprintf (_n( '%d item', '%d items', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?> - <?php echo WC()->cart->get_cart_total(); ?></a>
	<?php

	$fragments['a.cart-contents'] = ob_get_clean();

	return $fragments;
}

//Rearrange the Fields in the Checkout Billing Details Form
/*
add_filter("woocommerce_checkout_fields", "new_order_fields");

function new_order_fields($fields) {

$order_list = array(
    "billing_postcode",
    "billing_first_name",
    "billing_last_name",
    "billing_email",
    "billing_phone",
    "billing_company",
    "billing_address_1",
    "billing_address_2",
    "billing_country"

);
foreach($order_list as $field)
{
    $ordered_fields[$field] = $fields["billing"][$field];
}

$fields["billing"] = $ordered_fields;
return $fields;

}
*/

add_action( 'template_redirect', 'wc_custom_redirect_after_purchase' );
function wc_custom_redirect_after_purchase() {
	global $wp;

	if ( is_checkout() && ! empty( $wp->query_vars['order-received'] ) ) {
		wp_redirect( 'http://unruhfurniture.com/thank-you-purchase' );
		exit;
	}
}

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {
    //unset($fields['billing']['billing_first_name']);
    //unset($fields['billing']['billing_last_name']);
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_city']);
    //unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_state']);
    //unset($fields['billing']['billing_phone']);
    //unset($fields['billing']['billing_email']);
    //unset($fields['order']['order_comments']);
    unset($fields['shipping']['shipping_company']);
    unset($fields['shipping']['shipping_phone']);
    unset($fields['shipping']['shipping_email']);
    unset($fields['shipping']['shipping_country']);
    unset($fields['shipping']['shipping_first_name']);
    unset($fields['shipping']['shipping_last_name']);
    unset($fields['shipping']['shipping_postcode']);
    return $fields;
}

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

// change login styles
add_action("login_head", "my_login_head");

function my_login_head() {
	echo "
	<style>
	body.login #login h1 a {
		background: url('".get_bloginfo('template_url')."/assets/img/login-image.png') no-repeat center top transparent;
		height: 277px;
		width: 500px;
		margin-bottom: 0;
	}
	#login {
		width: 500px;
	}
	.login label {
		font-family: 'edmondsans-regular', sans-serif;
		text-transform: uppercase;
	}
	.login form .input, .login input[type=text] {
		padding: 10px;
	}
	.login form {
		margin-top: 0;
		box-shadow: none;
	}
	.forgetmenot {
		display: none;
	}
	.login #login_error, .login .message {
		border-left: 4px solid #00c8c8;
		margin: 10px 0;
	}
	.wp-core-ui .button-group.button-large .button, .wp-core-ui .button.button-large {
		height: 50px;
		line-height: 28px;
		padding: 0;
	}
	.wp-core-ui .button-primary {
		font-family: 'edmondsans-regular', sans-serif;
		text-transform: uppercase;
		background: #5a4a42;
		border-color: #5a4a42;
		box-shadow: none;
		text-shadow: none;
		width: 100%;
		border-radius: 0;
		margin-top: 10px;
	}
	.wp-core-ui .button-primary:hover {
		background: #C8B499;
		border-color: #C8B499;
		box-shadow: none;
		text-shadow: none;
	}
	.wp-core-ui .button-primary:focus {
		background: #5a4a42;
		border-color: #5a4a42;
		box-shadow: none;
	}
	input[type=checkbox]:checked:before {
		color: #444344;
	}
	input[type=checkbox]:focus {
		border-color: #444344;
		box-shadow: none;
	}
	#backtoblog {
		display: none;
	}
	#nav {
		display: none;
	}
	</style>
	";
}


// Display Fields
add_action( 'woocommerce_product_options_general_product_data', 'woo_add_custom_general_fields' );

function woo_add_custom_general_fields() {

  global $woocommerce, $post;

  echo '<div class="options_group">';

  // Text Field
woocommerce_wp_text_input(
	array(
		'id'          => '_text_field',
		'label'       => __( 'Product Video URL', 'woocommerce' ),
		'placeholder' => 'http://',
		'desc_tip'    => 'true',
		'description' => __( 'Enter the custom value here.', 'woocommerce' )
	)
);

  echo '</div>';

}

function woo_add_custom_general_fields_save( $post_id ){

	// Text Field
	$woocommerce_text_field = $_POST['_text_field'];
	if( !empty( $woocommerce_text_field ) )
		update_post_meta( $post_id, '_text_field', esc_attr( $woocommerce_text_field ) );

}

// Save Fields
add_action( 'woocommerce_process_product_meta', 'woo_add_custom_general_fields_save' );

function product_video_functions() {
		return '
		<a href="#" class="button product-video">Product Video</a>
		<script>
		  $(document).ready(function(){
			  var iframe = $("#product-video")[0];
			  var player = $f(iframe);
			  $(".button.product-video").click(function(){
			    $(".video-overlay").css("left", 0)
			    $(".video-overlay").addClass("show")
			    $(".screen").addClass("show")
			    player.api("play");
			  })
			  $(".close-video-overlay").click(function(){
			    $(".video-overlay").removeClass("show")
			    $(".screen").removeClass("show")
			    setTimeout(function() {
			      $(".video-overlay").css("left", "-100%")
			    }, 300);
			    player.api("pause");
			  })
			});
	  	</script>
	  	';
}

function register_product_video(){
   add_shortcode('product-video', 'product_video_functions');
}
add_action( 'init', 'register_product_video');


function finish_options_functions() {
		return '<h3>Finish Options</h3>
		<a name="finish-lock" class="anchor"></a>
		<div id="finish-closeup"></div> 
		<ul id="finish-options" class="small-block-grid-3 medium-block-grid-3 large-block-grid-4" style="margin-top:40px;">
        <li><a class="classic-fir" href="#finish-lock"><img src="' .get_bloginfo('template_url'). '/assets/img/_finishes/classic-fir-icon.png"/><span>Classic Fir</span></a></li>
        <li><a class="clear-coat" href="#finish-lock"><img src="' .get_bloginfo('template_url'). '/assets/img/_finishes/clear-coat-icon.png"/><span>Clear Coat</span></a></li>
        <li><a class="deep-mocha" href="#finish-lock"><img src="' .get_bloginfo('template_url'). '/assets/img/_finishes/deep-mocha-icon.png"/><span>Deep Mocha</span></a></li>
        <li><a class="faded-ebony" href="#finish-lock"><img src="' .get_bloginfo('template_url'). '/assets/img/_finishes/faded-ebony-icon.png"/><span>Faded Ebony</span></a></li>
        <li><a class="weathered-oak" href="#finish-lock"><img src="' .get_bloginfo('template_url'). '/assets/img/_finishes/weathered-oak-icon.png"/><span>Weathered Oak</span></a></li>
        <li><a class="americano-black" href="#finish-lock"><img src="' .get_bloginfo('template_url'). '/assets/img/_finishes/americano-black-icon.png"/><span>Americano Black</span></a></li>
        <li><a class="antique-cream" href="#finish-lock"><img src="' .get_bloginfo('template_url'). '/assets/img/_finishes/antique-cream-icon.png"/><span>Antique Cream</span></a></li>
        <li><a class="galveston-gray" href="#finish-lock"><img src="' .get_bloginfo('template_url'). '/assets/img/_finishes/galveston-gray-icon.png"/><span>Galveston Gray</span></a></li>
        <li><a class="sage" href="#finish-lock"><img src="' .get_bloginfo('template_url'). '/assets/img/_finishes/sage-icon.png"/><span>Sage</span></a></li>
        <li><a class="teal" href="#finish-lock"><img src="' .get_bloginfo('template_url'). '/assets/img/_finishes/teal-icon.png"/><span>Teal</span></a></li>
        <li><a class="barnyard-red" href="#finish-lock"><img src="' .get_bloginfo('template_url'). '/assets/img/_finishes/barnyard-red-icon.png"/><span>Barn Red</span></a></li>
        <li><a class="deep-sea-gray" href="#finish-lock"><img src="' .get_bloginfo('template_url'). '/assets/img/_finishes/deep-sea-gray-icon.png"/><span>Deep Sea Gray</span></a></li>
        </ul>';
}

function register_finish_options(){
   add_shortcode('finish-options', 'finish_options_functions');
}
add_action( 'init', 'register_finish_options');

// Remove Product Options
add_filter( 'woocommerce_loop_add_to_cart_link', function( $product ) {
	global $product;

	if ( is_shop() && 'variable' === $product->product_type ) {
		return '';
	} else {
		return '';
	}
} );
