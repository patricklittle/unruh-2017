<?php

// Custom Product Page for Unruh

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;

?>
<li <?php post_class(); ?>>

	<?php //do_action( 'woocommerce_before_shop_loop_item' ); ?>

			<div class="archive-product-wrap">
				<div class="archive-product-image">
					<?php do_action( 'woocommerce_before_shop_loop_item_title' ); // Image ?>
				</div>
				<div class="archive-product-description">
					<a href="<?php the_permalink(); ?>">
						<?php do_action( 'woocommerce_shop_loop_item_title' ); // Title ?>
					</a>
					<?php do_action( 'woocommerce_after_shop_loop_item_title' ); // Price ?>
					<a class="button" href="<?php the_permalink(); ?>">View Item</a>
					<?php do_action( 'woocommerce_after_shop_loop_item' ); // Button ?>
				</div>
			</div>

</li>
<?php /*
// New Site

<li class="post-3464 product type-product status-publish has-post-thumbnail product_cat-desk product_cat-desk-wo-multiple-drawers product_cat-office product_shipping_class-office-desk-w-1-drawer last instock taxable shipping-taxable purchasable product-type-simple">

	<a href="https://unruhfurniture.wpengine.com/product/bristol-desk/" class="woocommerce-LoopProduct-link"></a>
	<div class="archive-product-wrap">
		<a href="https://unruhfurniture.wpengine.com/product/bristol-desk/" class="woocommerce-LoopProduct-link">
			<div class="archive-product-image">
				<img width="900" height="601" src="https://unruhfurniture.wpengine.com/wp-content/uploads/2016/01/W-BristolDesk-2-900x601.png" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="Southern Living Style Bristol Desk" title="Southern Living Style Bristol Desk" srcset="https://unruhfurniture.wpengine.com/wp-content/uploads/2016/01/W-BristolDesk-2-300x200.png 300w, https://unruhfurniture.wpengine.com/wp-content/uploads/2016/01/W-BristolDesk-2-768x513.png 768w, https://unruhfurniture.wpengine.com/wp-content/uploads/2016/01/W-BristolDesk-2-1024x684.png 1024w, https://unruhfurniture.wpengine.com/wp-content/uploads/2016/01/W-BristolDesk-2-1599x1067.png 1599w, https://unruhfurniture.wpengine.com/wp-content/uploads/2016/01/W-BristolDesk-2-900x601.png 900w, https://unruhfurniture.wpengine.com/wp-content/uploads/2016/01/W-BristolDesk-2-320x214.png 320w, https://unruhfurniture.wpengine.com/wp-content/uploads/2016/01/W-BristolDesk-2.png 1600w" sizes="(max-width: 900px) 100vw, 900px">
			</div>
		</a>
		<div class="archive-product-description">
			<a href="https://unruhfurniture.wpengine.com/product/bristol-desk/" class="woocommerce-LoopProduct-link"></a>
			<a href="https://unruhfurniture.wpengine.com/product/bristol-desk/">
				<h2 class="woocommerce-loop-product__title">Bristol Desk</h2>					
			</a>
		
			<span class="price">
				<span class="woocommerce-Price-amount amount">
					<span class="woocommerce-Price-currencySymbol">$</span>1499
				</span>
			</span>
			<a class="button" href="https://unruhfurniture.wpengine.com/product/bristol-desk/">View Item</a>
			<a rel="nofollow" href="https://unruhfurniture.wpengine.com/product/bristol-desk/" data-quantity="1" data-product_id="3464" data-product_sku="" class="button product_type_simple ">Select options</a>	
		</div>
	</div>

</li>

<li class="post-734 product type-product status-publish has-post-thumbnail product_cat-bedroom product_cat-box-piece-regular-height product_cat-box-piece-regular-length product_cat-dresser product_shipping_class-bedroom-dresser taxable shipping-taxable purchasable product-type-simple product-cat-bedroom product-cat-box-piece-regular-height product-cat-box-piece-regular-length product-cat-dresser instock">

	<a href="https://unruhfurniture.com/product/jackson-dresser/">
			</a><div class="archive-product-wrap"><a href="https://unruhfurniture.com/product/jackson-dresser/">
				<div class="archive-product-image">
					<img width="900" height="601" src="https://unruhfurniture.com/wp-content/uploads/2016/01/W-JacksonDresser-900x601.png" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="Southern Living Jackson Dresser" srcset="https://unruhfurniture.com/wp-content/uploads/2016/01/W-JacksonDresser-300x200.png 300w, https://unruhfurniture.com/wp-content/uploads/2016/01/W-JacksonDresser-768x513.png 768w, https://unruhfurniture.com/wp-content/uploads/2016/01/W-JacksonDresser-1024x684.png 1024w, https://unruhfurniture.com/wp-content/uploads/2016/01/W-JacksonDresser-1599x1067.png 1599w, https://unruhfurniture.com/wp-content/uploads/2016/01/W-JacksonDresser-900x601.png 900w, https://unruhfurniture.com/wp-content/uploads/2016/01/W-JacksonDresser-320x214.png 320w, https://unruhfurniture.com/wp-content/uploads/2016/01/W-JacksonDresser.png 1600w" sizes="(max-width: 900px) 100vw, 900px">				</div>
				</a><div class="archive-product-description"><a href="https://unruhfurniture.com/product/jackson-dresser/">
					</a><a href="https://unruhfurniture.com/product/jackson-dresser/">
						<h3>Jackson Dresser</h3>					</a>
					
	<span class="price"><span class="amount">$1699</span></span>
					<a class="button" href="https://unruhfurniture.com/product/jackson-dresser/">View Item</a>
					<a rel="nofollow" href="https://unruhfurniture.com/product/jackson-dresser/" data-quantity="1" data-product_id="734" data-product_sku="" class="button product_type_simple add_to_cart_button">Select options</a>				</div>
			</div>

</li>

