<!--
<?php if (is_front_page()) { ?>
		<div class="wrapper">
			<div class="screen" id="screen-1" data-video="<?php $options = get_option('sample_theme_options'); echo $options['ambient-url-1']; ?>">
	            <div class="big-image" style="background:url(<?php $options = get_option('sample_theme_options'); echo $options['image-url-1']; ?>) center center no-repeat;background-size:cover;"></div>
	            <div class="big-image-mobile" style="background:url(<?php $options = get_option('sample_theme_options'); echo $options['mobile-image-url-1']; ?>) center center no-repeat;background-size:cover;"></div>
	            <div class="video-title-wrap">
		            <div class="video-title">
			            <h3><?php $options = get_option('sample_theme_options'); echo $options['video-content-header-1']; ?></h3>
						<span><?php $options = get_option('sample_theme_options'); echo $options['video-content-1']; ?></span>
						<hr/>
			        </div>
			        <a class="video-title-button kids"><?php $options = get_option('sample_theme_options'); echo stripslashes($options['video-button-1']); ?> ></a>
			        <a class="video-title-button alternate" href="/showroom">See our Showroom ></a>
	        	</div>
		        <div class="video-overlay">
			      <a class="close-video-overlay">×</a>
			      <div class="video-container">
					  <div class="flex-video widescreen vimeo">
					    <iframe id="kids" src="<?php $options = get_option('sample_theme_options'); echo $options['audio-url-1']; ?>?api=1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					  </div>
				  </div>
	            </div>
	        </div>
	        <script>
			  $(document).ready(function(){
				  // Iframe/player variables
				  var iframe = $('#kids')[0];
				  var player = $f(iframe);
				  // Open on play
				  $('.video-title-button.kids').click(function(){
				    $('.video-overlay').css('left', 0)
				    $('.video-overlay').addClass('show')
				    $('.screen').addClass('show')
				    player.api("play");
				  })
				  // Closes on click outside
				  $('.close-video-overlay,.next-icon,.prev-icon').click(function(){
				    $('.video-overlay').removeClass('show')
				    $('.screen').removeClass('show')
				    setTimeout(function() {
				      $('.video-overlay').css('left', '-100%')
				    }, 300);
				    player.api("pause");
				  })
				});
		  	</script>
		  	<div class="screen" id="screen-2" data-video="<?php $options = get_option('sample_theme_options'); echo $options['ambient-url-2']; ?>">
	            <div class="big-image" style="background:url(<?php $options = get_option('sample_theme_options'); echo $options['image-url-2']; ?>) center center no-repeat;background-size:cover;"></div>
	            <div class="big-image-mobile" style="background:url(<?php $options = get_option('sample_theme_options'); echo $options['mobile-image-url-2']; ?>) center center no-repeat;background-size:cover;"></div>
	            <div class="video-title-wrap">
		            <div class="video-title">
			            <h3><?php $options = get_option('sample_theme_options'); echo $options['video-content-header-2']; ?></h3>
						<span><?php $options = get_option('sample_theme_options'); echo $options['video-content-2']; ?></span>
						<hr/>
			        </div>
			        <a class="video-title-button hayley"><?php $options = get_option('sample_theme_options'); echo stripslashes($options['video-button-2']); ?> ></a>
			        <a class="video-title-button alternate" href="/showroom">See our Showroom ></a>
	            </div>
		        <div class="video-overlay">
			      <a class="close-video-overlay">×</a>
			      <div class="video-container">
					  <div class="flex-video widescreen vimeo">
					    <iframe id="hayley" src="<?php $options = get_option('sample_theme_options'); echo $options['audio-url-2']; ?>?api=1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					  </div>
				  </div>
	            </div>
	        </div>
	        <script>
			  $(document).ready(function(){
				  // Iframe/player variables
				  var iframe = $('#hayley')[0];
				  var player = $f(iframe);
				  // Open on play
				  $('.video-title-button.hayley').click(function(){
				    $('.video-overlay').css('left', 0)
				    $('.video-overlay').addClass('show')
				    $('.screen').addClass('show')
				    player.api("play");
				  })
				  // Closes on click outside
				  $('.close-video-overlay,.next-icon,.prev-icon').click(function(){
				    $('.video-overlay').removeClass('show')
				    $('.screen').removeClass('show')
				    setTimeout(function() {
				      $('.video-overlay').css('left', '-100%')
				    }, 300);
				    player.api("pause");
				  })
				});
		  	</script>
	        <div class="screen" id="screen-3" data-video="<?php $options = get_option('sample_theme_options'); echo $options['ambient-url-3']; ?>">
				<div class="big-image" style="background:url(<?php $options = get_option('sample_theme_options'); echo $options['image-url-3']; ?>) center center no-repeat;background-size:cover;"></div>
				<div class="big-image-mobile" style="background:url(<?php $options = get_option('sample_theme_options'); echo $options['mobile-image-url-3']; ?>) center center no-repeat;background-size:cover;"></div>
	            <div class="video-title-wrap">
		            <div class="video-title">
			            <h3><?php $options = get_option('sample_theme_options'); echo $options['video-content-header-3']; ?></h3>
						<span><?php $options = get_option('sample_theme_options'); echo $options['video-content-3']; ?></span>
						<hr/>
			        </div>
			        <a class="video-title-button alec"><?php $options = get_option('sample_theme_options'); echo stripslashes($options['video-button-3']); ?> ></a>
			        <a class="video-title-button alternate" href="/showroom">See our Showroom ></a>
	            </div>
	            <div class="video-overlay">
			      <a class="close-video-overlay">×</a>
			      <div class="video-container">
					  <div class="flex-video widescreen vimeo">
					    <iframe id="alec" src="<?php $options = get_option('sample_theme_options'); echo $options['audio-url-3']; ?>?api=1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					  </div>
				  </div>
	            </div>
	        </div>
	        <script>
			  $(document).ready(function(){
				  // Iframe/player variables
				  var iframe = $('#alec')[0];
				  var player = $f(iframe);
				  // Open on play
				  $('.video-title-button.alec').click(function(){
				    $('.video-overlay').css('left', 0)
				    $('.video-overlay').addClass('show')
				    $('.screen').addClass('show')
				    player.api("play");
				  })
				  // Closes on click outside
				  $('.close-video-overlay,.next-icon,.prev-icon').click(function(){
				    $('.video-overlay').removeClass('show')
				    $('.screen').removeClass('show')
				    setTimeout(function() {
				      $('.video-overlay').css('left', '-100%')
				    }, 300);
				    player.api("pause");
				  })
				});
		  	</script>
	    </div>

    <div id="next-btn">
        <a href="#" class="next-icon"></a>
    </div>
    <div id="prev-btn">
        <a href="#" class="prev-icon"></a>
    </div>
	<script>
        $(function() {

            // Use Modernizr to detect for touch devices,
            // which don't support autoplay and may have less bandwidth,
            // so just give them the poster images instead
            var screenIndex = 1,
                numScreens = $('.screen').length,
                isTransitioning = false,
                transitionDur = 1000,
                BV,
                videoPlayer,
                isTouch = Modernizr.touch,
                $bigImage = $('.big-image'),
                $window = $(window);

            if (!isTouch) {
                // initialize BigVideo
                BV = new $.BigVideo({forceAutoplay:isTouch});
                BV.init();
                showVideo();

                BV.getPlayer().addEvent('loadeddata', function() {
                    onVideoLoaded();
                });

                // adjust image positioning so it lines up with video
                $bigImage
                    .css('position','relative')
                    .imagesLoaded(adjustImagePositioning);
                // and on window resize
                $window.on('resize', adjustImagePositioning);
            }

            // Next button click goes to next div
            $('#next-btn').on('click', function(e) {
                e.preventDefault();
                if (!isTransitioning) {
                    next();
                }
            });
            $('#prev-btn').on('click', function(e) {
                e.preventDefault();
                if (!isTransitioning) {
                    previous();
                }
            });

            function showVideo() {
                BV.show($('#screen-'+screenIndex).attr('data-video'),{ambient:true});
            }

            function next() {
				isTransitioning = true;
				// update video index, reset image opacity if starting over
				if (screenIndex === numScreens) {
				screenIndex = 1;
				} else {
				screenIndex++;
				}
				if (!isTouch) {
				$('#big-video-wrap').transit({'left':'-100%'},transitionDur)
				}
				$bigImage.css('opacity',1);
				$('.wrapper').transit(
				{'left':'-'+(100*(screenIndex-1))+'%'},
				transitionDur,
				onTransitionComplete);
			}

			function previous() {
				isTransitioning = true;
				// update video index, reset image opacity if starting over
				if (screenIndex === 1) {
				screenIndex = numScreens;
				} else {
				screenIndex--;
				}
				if (!isTouch) {
				$('#big-video-wrap').transit({'left':'100%'},transitionDur)
				}
				$bigImage.css('opacity',1);
				$('.wrapper').transit(
				{'left':'-'+(100*(screenIndex-1))+'%'},
				transitionDur,
				onTransitionComplete);
			}

            function onVideoLoaded() {
                $('#screen-'+screenIndex).find('.big-image').transit({'opacity':0},500)
            }

            function onTransitionComplete() {
                isTransitioning = false;
                if (!isTouch) {
                    $('#big-video-wrap').css('left',0);
                    showVideo();
                }
            }

            function adjustImagePositioning() {
                $bigImage.each(function(){
                    var $img = $(this),
                        img = new Image();

                    img.src = $img.attr('src');

                    var windowWidth = $window.width(),
                        windowHeight = $window.height(),
                        r_w = windowHeight / windowWidth,
                        i_w = img.width,
                        i_h = img.height,
                        r_i = i_h / i_w,
                        new_w, new_h, new_left, new_top;

                    if( r_w > r_i ) {
                        new_h   = windowHeight;
                        new_w   = windowHeight / r_i;
                    }
                    else {
                        new_h   = windowWidth * r_i;
                        new_w   = windowWidth;
                    }

                    $img.css({
                        width   : new_w,
                        height  : new_h,
                        left    : ( windowWidth - new_w ) / 2,
                        top     : ( windowHeight - new_h ) / 2
                    })

                });

            }
        });
    </script>

<?php } else { ?>
<?php } ?>
-->