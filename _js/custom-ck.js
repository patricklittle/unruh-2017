$(function() {
    $("a[href*=#]:not([href=#])").click(function() {
        if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
            var e = $(this.hash);
            e = e.length ? e : $("[name=" + this.hash.slice(1) + "]");
            if (e.length) {
                $("html,body").animate({
                    scrollTop: e.offset().top
                }, 500);
                return !1
            }
        }
    })
});
$(window).scroll(function() {
    var e = $(this).scrollTop();
    e >= 30 ? $(".fixed").addClass("solid") : $(".fixed").removeClass("solid")
});
$(".flex-caption").each(function() {
    var e = $(this).parent().height();
    $(this).height(e)
});
$(document).ready(function() {
    $(".dropdown-content-wrap.solutions").hover(function() {
        $(".dropdown-link.solutions").addClass("border")
    }, function() {
        $(".dropdown-link.solutions").removeClass("border")
    });
    $(".dropdown-content-wrap.case-studies").hover(function() {
        $(".dropdown-link.case-studies").addClass("border")
    }, function() {
        $(".dropdown-link.case-studies").removeClass("border")
    });
    $(".dropdown-content-wrap.company").hover(function() {
        $(".dropdown-link.company").addClass("border")
    }, function() {
        $(".dropdown-link.company").removeClass("border")
    });
    $(".dropdown-content-wrap.thought-leadership").hover(function() {
        $(".dropdown-link.thought-leadership").addClass("border")
    }, function() {
        $(".dropdown-link.thought-leadership").removeClass("border")
    });
    $(".dropdown-content-wrap.shop").hover(function() {
        $(".dropdown-link.shop").addClass("border")
    }, function() {
        $(".dropdown-link.shop").removeClass("border")
    });
    $(".hamburger").click(function() {
        $(this).toggleClass("open")
    });
    $(".close-button").click(function() {
        $(".questions-form").removeClass("show");
        $(".visit-form").removeClass("show")
    });
    $(".questions-link").click(function() {
        $(".questions-form").toggleClass("show")
    });
    $(".visit-link").click(function() {
        $(".visit-form").toggleClass("show")
    });
    $(".pete-dog").hover(function() {
        $(".pete-dog-text").toggleClass("show")
    });
    $(".pete-dog").hover(function() {
        $(this).toggleClass("show")
    });
    $(".gentlemen-section").hover(function() {
        $(".gentlemen-section-text").toggleClass("show")
    });
    $(".gentlemen-section").hover(function() {
        $(this).toggleClass("show")
    });
    $(".video-title-button").click(function() {
        $("body").toggleClass("modal-open")
    });
    // $(".appointment").hover(function() {
    //     $(".appointment-bottom").toggleClass("show")
    // });
    $(".close-reveal-modal").click(function() {
        $("body").toggleClass("modal-open")
    });
    $(".form-link").click(function() {
        $(".first-form-section").addClass("hide")
    });
    $(".overlay-close").click(function() {
        $(".overlay-appointment").removeClass("open")
    })

    // Finish Options
    $(".americano-black, .antique-cream, .barnyard-red, .blue, .classic-fir, .clear-coat, .deep-mocha, .deep-sea-gray, .faded-ebony, .galveston-gray, .mint, .mountainview-green, .sage, .teal, .weathered-oak").on("click", function() {
        $("#finish-closeup").removeClass().addClass($(this).attr("class"))
    })

    $(".finish-options-button").click(function() {
        $(".finish-wrap").slideToggle("fast")
    })

    
    $(".form-link").click(function() {
        var e = $(this).attr("data-divider");
        $(".myDivider").each(function() {
            $(this).hide()
        });
        $(e).show()
    })
});
$(function() {
    $(".banner").css({
        height: $(window).height() + "px"
    });
    $(window).resize(function() {
        $(".banner").css({
            height: $(window).height() + "px"
        })
    });
    $(".cover").css({
        height: $(window).height() + "px"
    });
    $(window).resize(function() {
        $(".cover").css({
            height: $(window).height() + "px"
        })
    })
});