$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 500);
        return false;
      }
    }
  });
});


$(window).scroll (function () {
	var sT = $(this).scrollTop();
	    if (sT >= 30){
	        $('.fixed').addClass('solid')
	    } else {
	        $('.fixed').removeClass('solid')
	      }
});
$(window).scroll (function () {
	var sT = $(this).scrollTop();
	    if (sT >= 850){
	        $('#big-video-wrap').addClass('dissapear')
	    } else {
	        $('#big-video-wrap').removeClass('dissapear')
	      }
});

$('.flex-caption').each(function(){
      var parentHeight = $(this).parent().height();
      $(this).height(parentHeight);
});

$(document).ready(function() {
    $('.dropdown-content-wrap.solutions').hover(function(){
        $('.dropdown-link.solutions').addClass('border');
    },
    function(){
        $('.dropdown-link.solutions').removeClass('border');
    });

    $('.dropdown-content-wrap.case-studies').hover(function(){
        $('.dropdown-link.case-studies').addClass('border');
    },
    function(){
        $('.dropdown-link.case-studies').removeClass('border');
    });

    $('.dropdown-content-wrap.company').hover(function(){
        $('.dropdown-link.company').addClass('border');
    },
    function(){
        $('.dropdown-link.company').removeClass('border');
    });

    $('.dropdown-content-wrap.thought-leadership').hover(function(){
        $('.dropdown-link.thought-leadership').addClass('border');
    },
    function(){
        $('.dropdown-link.thought-leadership').removeClass('border');
    });

    $('.dropdown-content-wrap.shop').hover(function(){
        $('.dropdown-link.shop').addClass('border');
    },
    function(){
        $('.dropdown-link.shop').removeClass('border');
    });

    $(".hamburger").click(function() {
		$(this).toggleClass("open");
  	});
  	$(".close-button").click(function() {
		$('.questions-form').removeClass("show");
		$('.visit-form').removeClass("show");
  	});
  	$(".questions-link").click(function() {
		$('.questions-form').toggleClass("show");
  	});
  	$(".visit-link").click(function() {
		$('.visit-form').toggleClass("show");
  	});
});

$(document).ready(function() {
    $('.form-link').click(function() {
        // Get data attribute from clicked header
        var correspondingDiv = $(this).attr('data-divider');
        // Hide any open 'myDivider' dividers
        $('.myDivider').each(function() { $(this).hide(); })
        // Display the corresponding divider
        $(correspondingDiv).show();
    });
});


$(function(){
    $('.banner') .css({'height': (($(window).height()))+'px'});
    $(window).resize(function(){
        $('.banner') .css({'height': (($(window).height()))+'px'});
    });
    $('.cover') .css({'height': (($(window).height()))+'px'});
    $(window).resize(function(){
        $('.cover') .css({'height': (($(window).height()))+'px'});
    });
});