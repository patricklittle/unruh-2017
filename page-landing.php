<?php /* Template Name: Landing */ get_header(); ?>

<!-- <link rel='stylesheet' id='iswiper-css'  href='https://unruhfurniture.com/wp-content/themes/unruh/iswiper.css?ver=1.0.686035' type='text/css' media='all' /> -->

<div class="workshop">

    <style type="text/css">
        @media only screen and (max-width: 980px) {
            .workshop .video-box {
                background: url('<?php the_field('slider_mobile_background'); ?>') !important;
            }
            .videoWrapperTrigger,
            .overlayvideo {
                background: url('<?php the_field('background_image_mobile'); ?>') !important;
            }
        }
        @media only screen and (max-width: 768px) {
            #testmoslide1 {
                background: url('<?php the_field('slide_large_image_mobile_1'); ?>') !important;
                
            }
            #testmoslide2 {
                background: url('<?php the_field('slide_large_image_mobile_2'); ?>') !important;
            }
            #testmoslide3 {
                background: url('<?php the_field('slide_large_image_mobile_3'); ?>') !important;
            }
        }
    </style>

    <!-- top slider -->
    <div class="full-width-block home-featured2">
        <div class="overlay wrap">
            <div class="video-box background-cover">
                <div class="show-for-medium-up">
                    <?php if(get_field('slider_video_1')): ?>
                        <?php the_field('slider_video_1'); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="full-width-block-interior-video light wrap">
                <div class="workshop-wrap text-center">
                    <h1 class="workshop-headline"><?php the_field( 'slider_main_title'); ?></h1>
                    <p class="home-sub-text"><?php the_field('slider_sub_title'); ?></p>
                    
                    <div class="eds-on-scroll eds-scroll-hidden eds-scroll-visible animated fadeInUp duration2">

                        <a href="<?php the_field('slider_left_button_link'); ?>" class="button-green-stroke">
                            <?php the_field( 'slider_left_button_title'); ?>
                        </a>
                    <?php if(get_field('slider_video_2')): ?>
                        <script src="https://fast.wistia.com/embed/medias/<?php the_field('slider_video_2'); ?>.jsonp" async></script>
                        <script src="https://fast.wistia.com/assets/external/E-v1.js" async></script>
                        <span class="wistia_embed wistia_async_<?php the_field('slider_video_2'); ?> popover=true popoverContent=link" style="display:inline">
                            <a href="#" class="button-video video-anchor"><img src="https://unruhfurniture.com/wp-content/themes/unruh/play-btn.png" style="vertical-align:middle;margin-top:-4px;" width="20px"> <?php the_field( 'slider_right_button_title'); ?></a>
                        </span>
                    <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---/top slider--->
    
    <?php if(get_field( 'homepage_logos')) : ?>
    <div class="full-width-block dark logox for-desktop-only">
        <div class="full-width-block-interior-logos clearfix">
             <img src="<?php the_field('homepage_logos'); ?>" />
        </div>
    </div>
    <?php endif; ?>
    <!---/full-width-block dark--->

    <!---first text block area--->
    <div class="full-width-block">
        <div class="full-width-block-interior dark text-center">
                <div class="online-title-quote">
                    <?php the_field('large_heading'); ?>
                </div>

                <div class="one-fourth first bottom-pad mobile-pad-ten">
                    <?php the_field( 'text_1'); ?>
                </div>
                <div class="one-fourth bottom-pad mobile-pad-ten">
                    <?php the_field( 'text_2'); ?>
                </div>
                <div class="one-fourth bottom-pad mobile-pad-ten">
                    <?php the_field( 'text_3'); ?>
                </div>
                <div class="one-fourth bottom-pad mobile-pad-ten">
                    <?php the_field( 'text_4'); ?>
                </div>

                <p class="first">
                    <a href="<?php the_field('button_link'); ?>" class="button-green">
                        <?php the_field( 'button_heading'); ?>
                    </a>
                </p>
        </div>
    </div>
    <!---/first text block area--->

    <!---video block--->
        
    <?php if(get_field('customize-video')) :?>
        <div class="customize-video">
            <?php the_field('customize-video'); ?>
        </div>
    <?php endif; ?>

    <?php if(get_field('2nd_text_block')) : ?>
        <div class="full-width-block dark gfdfysdf" style="background-color:#fff">
            <div class="full-width-block-interior">
                <?php the_field( '2nd_text_block'); ?>
            </div>
        </div>
    <?php endif; ?>
    
    <?php get_template_part( 'inc/reviews', 'slider' ); ?>
    
    <div class="full-width-block" id="schedule">
        <div class="full-width-block-interior">
            <div class="sform">
                <?php the_field( 'schedule_text_block'); ?>
            </div>
        </div>
    </div>

    <script type='text/javascript' src='https://unruhfurniture.com/wp-content/themes/unruh/iswiper.js?ver=3.1.2'></script>
    <script type='text/javascript' src='https://unruhfurniture.com/wp-content/themes/unruh/global.js?ver=1.0.4'></script>
    <script type='text/javascript' src='https://unruhfurniture.com/wp-content/themes/unruh/jquery.localScroll.min.js?ver=1.2.8b'></script>

</div>
<!--workshop-->
<?php get_footer(); ?>