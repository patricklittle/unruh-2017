jQuery(function( $ ){

	$(".site-header").after('<div class="bumper"></div>');
	/*
	$(document).on("scroll", function(){
		if($(document).scrollTop() > 100){
			$(".site-header").addClass("shrink");
			updateSliderMargin();
		}
		else
		{
			$(".site-header").removeClass("shrink");
			updateSliderMargin();
		}
	});*/

    $('.home-featured .wrap') .css({'height': (($(window).height()))+'px'});
    $(window).resize(function(){
        $('.home-featured .wrap') .css({'height': (($(window).height()))+'px'});
    });

    $(".home-featured .home-widgets-1").after('<a href="#home-widgets" class="arrow"></a>');
		$('.site-header').addClass('shrink');
		$.localScroll({
			offset: -$('.site-header').height(),
    	duration: 750
    });
		$('.site-header').removeClass('shrink');

	$(".menu-trigger").click(function(){
		$(".menu-contents").slideToggle();
	});

	$('a.video-anchor').on('click',function(){
		var iframe = $('#video').find('#mainVideo');
		$('#video').addClass('overlay');
			iframe.attr('src',iframe.data('src'));
	});

	$('.video #mainVideo').each(function(){
			$(this).data('src',$(this).attr('src') + '?autoplay=1');
			$(this).attr('src','');
	});

	$('[data-video]').on('click',function(){
		target = $('#' + $(this).data('video'));
		var iframe = target.find('#mainVideo');
		target.addClass('overlay');
		iframe.attr('src',iframe.data('src'));
	});

	$('.video .overlay-x').on('click',function(){
		var video = $(this).closest('.video');
		var iframe = video.find('#mainVideo');
		video.removeClass('overlay');
		iframe.attr('src','');
	});

	$('#video .overlay-x').on('click',function(){
		var iframe = $('#video').find('#mainVideo');
		$('#video').removeClass('overlay');
		if ($('#video .videoWrapper').hasClass('videoWrapperHide')){
			iframe.attr('src','');
		}
	});

	$('.videoWrapperTrigger a').on('click',function(){
		var trigger = $(this).closest('.videoWrapperTrigger');
		var wrapper = trigger.prev().find('.videoWrapper');
		var iframe = wrapper.find('#mainVideo');
		trigger.addClass('videoWrapperTriggerHide');
		wrapper.removeClass('videoWrapperHide');
		if (iframe.attr('src')==''){
			iframe.attr('src',iframe.data('src'));
		}
	});

	var contentSlider = new Swiper ('.swiper-container-content');
	var testimonialSlider = new Swiper ('.swiper-container-testimonial');
	
	$('.slider-sales-button').on('click',function(){
		contentSlider.slideTo($(this).index());
	})

	$('.quote-circle').on('click',function(){
		testimonialSlider.slideTo($(this).parent().index());
	})

	$('.video-trigger').on('click',function(){
		var iframe = $('#video').find('#mainVideo').attr('src',$(this).data('video-src'));
		$('#video').addClass('overlay');
	});

	$('.module-nav-trigger').on('click',function(){
		$('.module-nav').toggleClass('active');
	});
});
