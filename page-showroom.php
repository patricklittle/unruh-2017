<?php
/*
Template Name: Showroom
*/
get_header(); ?>
<?php $url = 'https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
if (strpos($url,'offer=1') !== false) { echo '<style>#toggle-appointment-showroom{display:none;}.showroom-intro{display:none;}</style>'; }
if (strpos($url,'offer=2') !== false) { echo '<style>#toggle-appointment-showroom{display:none;}.showroom-intro{display:none;}</style>'; }
if (strpos($url,'offer=3') !== false) { echo '<style>#toggle-appointment-showroom{display:none;}.showroom-intro{display:none;}</style>'; }
?>
	<div class="video-overlay">
      <a class="close-video-overlay">&#215;</a>
      <div class="video-container">
		  <div class="flex-video widescreen vimeo">
		    <iframe id="showroom" src="<?php $options = get_option('sample_theme_options'); echo $options['audio-url-showroom']; ?>?api=1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		  </div>
	  </div>
    </div>
	<div class="small-12 large-12 columns" id="content" role="main">
		<div class="background-video mobile" style="background:url('https://unruhfurniture.com/wp-content/uploads/2016/01/mobile-showroom-bg.jpg') center center no-repeat;background-size:cover;">
			<div class="row">
				<div class="large-6 columns">
					<h1 class="white">Come visit our showroom.</h1>
					<p class="white showroom-intro">Made to order, made to last and made by hand in Kansas City. We invite you to visit our showroom in person. Watch our craftsman work, then sit with us and design the perfect addition to your family’s home.</p>
					<a class="button showroom" style="margin-top:10px;display:inline-block;">Watch our showroom video</a>
					<?php if (strpos($url,'offer=1') !== false) { echo '<a class="button showroom-offer" href="'; $options = get_option('sample_theme_options'); echo $options['offer-1-url']; echo '">'; $options = get_option('sample_theme_options'); echo $options['offer-1-text']; echo '</a>'; } ?>
					<?php if (strpos($url,'offer=2') !== false) { echo '<a class="button showroom-offer" href="'; $options = get_option('sample_theme_options'); echo $options['offer-2-url']; echo '">'; $options = get_option('sample_theme_options'); echo $options['offer-2-text']; echo '</a>'; } ?>
					<?php if (strpos($url,'offer=3') !== false) { echo '<a class="button showroom-offer" href="'; $options = get_option('sample_theme_options'); echo $options['offer-3-url']; echo '">'; $options = get_option('sample_theme_options'); echo $options['offer-3-text']; echo '</a>'; } ?>
				</div>
			</div>
		</div>
		<?php
		if ( !wp_is_mobile() ) { ?>
		<div class="showroom-wrapper cover"></div>
		<div class="background-video desktop">
			<div class="row">
				<div class="large-6 columns video-text">
					<a class="anchor" name="content"></a>
					<h1 class="white">Come visit our showroom.</h1>
					<p class="white showroom-intro">Made to order, made to last and made by hand in Kansas City. We invite you to visit our showroom in person. Watch our craftsman work, then sit with us and design the perfect addition to your family’s home.</p>
					<?php if (strpos($url,'offer=1') !== false) { echo '<p class="white">'; $options = get_option('sample_theme_options'); echo $options['offer-1-content']; echo '</p>'; }?>
					<?php if (strpos($url,'offer=2') !== false) { echo '<p class="white">'; $options = get_option('sample_theme_options'); echo $options['offer-2-content']; echo '</p>'; }?>
					<?php if (strpos($url,'offer=3') !== false) { echo '<p class="white">'; $options = get_option('sample_theme_options'); echo $options['offer-3-content']; echo '</p>'; }?>
					<a class="button showroom" style="margin-top:10px;display:inline-block;">Watch our showroom video</a>
					<?php if (strpos($url,'offer=1') !== false) { echo '<a class="button" style="margin-top:10px;margin-left:20px;display:inline-block;" href="'; $options = get_option('sample_theme_options'); echo $options['offer-1-url']; echo '">'; $options = get_option('sample_theme_options'); echo $options['offer-1-text']; echo '</a>'; } ?>
					<?php if (strpos($url,'offer=2') !== false) { echo '<a class="button" style="margin-top:10px;margin-left:20px;display:inline-block;" href="'; $options = get_option('sample_theme_options'); echo $options['offer-2-url']; echo '">'; $options = get_option('sample_theme_options'); echo $options['offer-2-text']; echo '</a>'; } ?>
					<?php if (strpos($url,'offer=3') !== false) { echo '<a class="button" style="margin-top:10px;margin-left:20px;display:inline-block;" href="'; $options = get_option('sample_theme_options'); echo $options['offer-3-url']; echo '">'; $options = get_option('sample_theme_options'); echo $options['offer-3-text']; echo '</a>'; } ?>
				</div>
			</div>
		</div>
		<div class="arrow-wrap">
			<a href="#content" class="arrow bounce"></a>
		</div>
		<script>
			var BV = new $.BigVideo({useFlashForFirefox:false, container:$('.background-video')});
				BV.init();
			        if (Modernizr.touch) {
			        BV.show('');
			    } else {
			        BV.show('<?php $options = get_option('sample_theme_options'); echo $options['ambient-url-showroom']; ?>',{ambient:true, altSource:''});
			    }
		</script>
	  	<?php } ?>
	  	<script>
		  $(document).ready(function(){
			  // Iframe/player variables
			  var iframe = $('#showroom')[0];
			  var player = $f(iframe);
			  // Open on play
			  $('.button.showroom').click(function(){
			    $('.video-overlay').css('left', 0)
			    $('.video-overlay').addClass('show')
			    $('.screen').addClass('show')
			    player.api("play");
			  })
			  // Closes on click outside
			  $('.close-video-overlay').click(function(){
			    $('.video-overlay').removeClass('show')
			    $('.screen').removeClass('show')
			    setTimeout(function() {
			      $('.video-overlay').css('left', '-100%')
			    }, 300);
			    player.api("pause");
			  })
			});
	  	</script>
		<?php while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile;?>
	</div>

<?php get_footer(); ?>