<?php get_header(); ?>

<div class="large-12 columns" id="content" role="main">
	<div class="padding-medium" data-parallax="scroll" data-image-src="<?php echo $upload_dir['baseurl']; ?>/2016/01/blog-header-bg.jpg" data-natural-width="1600" data-natural-height="527">
		<div class="row">
			<div class="large-12 columns">
				<h1 class="white">Blog</h1>
			</div>
		</div>
	</div>
		<div class="row">
			<div class="padding-small">
				<div class="medium-8 large-8 columns">
					<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'thumbnail') ); ?>
					<h1 style="margin-bottom:10px;"><?php the_title(); ?></h1>
					<?php echo the_excerpt(); ?>
					<img style="margin-bottom:60px;" src="<?php echo $url ?>" />
					<?php while (have_posts()) : the_post(); ?>
						<?php echo wpautop( $post->post_content ); ?>
						<div class="post-navigation">
							<div class="left">
								<?php previous_post_link( '<strong>%link</strong>' ); ?>
							</div>
							<div class="right">
								<?php next_post_link( '<strong>%link</strong>' ); ?>
							</div>
						</div>
					<?php endwhile;?>
				</div>
				<div class="medium-4 large-4 columns">
					<aside>
						<?php dynamic_sidebar( 'sidebar' ); ?>
					</aside>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
</div>

<?php get_footer(); ?>