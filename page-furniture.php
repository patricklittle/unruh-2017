<?php
/*
Template Name: Furniture
*/
get_header(); ?>

	<div class="large-12 columns" id="content" role="main">
		<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'thumbnail') ); ?>
			<?php if( !wp_is_mobile() ) { ?><div class="subpage-header" data-parallax="scroll" data-position="top" data-image-src="<?php echo $url ?>"><?php } ?>
				<?php if( wp_is_mobile() ) { ?> <div class="subpage-header" style="background:linear-gradient( rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7) ),url('<?php echo $url ?>') no-repeat center center;background-size:cover;"> <?php } ?>
				<div class="term-description">
					<?php echo the_excerpt(); ?>
				</div>
			</div>
		<div class="product-list large-centered columns">
			<?php while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile;?>
		</div>
	</div>

<?php get_footer(); ?>
