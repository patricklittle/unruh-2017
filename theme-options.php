<?php

add_action( 'admin_init', 'theme_options_init' );
add_action( 'admin_menu', 'theme_options_add_page' );

/**
 * Init plugin options to white list our options
 */
function theme_options_init(){
	register_setting( 'sample_options', 'sample_theme_options', 'theme_options_validate' );
}

/**
 * Load up the menu page
 */
function theme_options_add_page() {
	add_theme_page( __( 'Theme Options', 'sampletheme' ), __( 'Theme Options', 'sampletheme' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
}

/**
 * Create the options page
 */
function theme_options_do_page() {
	global $select_options, $radio_options;

	if ( ! isset( $_REQUEST['settings-updated'] ) )
		$_REQUEST['settings-updated'] = false;

	?>
	<div class="wrap">
		<?php screen_icon(); echo "<h2>" . get_current_theme() . __( ' Theme Options', 'sampletheme' ) . "</h2>"; ?>

		<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
		<div class="updated fade"><p><strong><?php _e( 'Options saved', 'sampletheme' ); ?></strong></p></div>
		<?php endif; ?>

		<form method="post" action="options.php">
			<?php settings_fields( 'sample_options' ); ?>
			<?php $options = get_option( 'sample_theme_options' ); ?>

			<h2>Homepage Options</h2>
			<hr/>
			<table class="form-table" style="margin-left:30px;">
				<h3>1st Video Options</h3>
				<tr valign="top"><th scope="row"><?php _e( 'Ambient Video URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[ambient-url-1]" class="regular-text" type="text" name="sample_theme_options[ambient-url-1]" value="<?php esc_attr_e( $options['ambient-url-1'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Audio Video URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[audio-url-1]" class="regular-text" type="text" name="sample_theme_options[audio-url-1]" value="<?php esc_attr_e( $options['audio-url-1'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Background Image URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[image-url-1]" class="regular-text" type="text" name="sample_theme_options[image-url-1]" value="<?php esc_attr_e( $options['image-url-1'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Mobile Background Image URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[mobile-image-url-1]" class="regular-text" type="text" name="sample_theme_options[mobile-image-url-1]" value="<?php esc_attr_e( $options['mobile-image-url-1'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Video Content Header', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[video-content-header-1]" class="regular-text" type="text" name="sample_theme_options[video-content-header-1]" value="<?php esc_attr_e( $options['video-content-header-1'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Video Content', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[video-content-1]" class="regular-text" type="text" name="sample_theme_options[video-content-1]" value="<?php esc_attr_e( $options['video-content-1'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Video Button Name', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[video-button-1]" class="regular-text" type="text" name="sample_theme_options[video-button-1]" value="<?php esc_attr_e( $options['video-button-1'] ); ?>" />
					</td>
				</tr>
			</table>

			<table class="form-table" style="margin-left:30px;">
				<h3>2nd Video Options</h3>
				<tr valign="top"><th scope="row"><?php _e( 'Ambient Video URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[ambient-url-2]" class="regular-text" type="text" name="sample_theme_options[ambient-url-2]" value="<?php esc_attr_e( $options['ambient-url-2'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Audio Video URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[audio-url-2]" class="regular-text" type="text" name="sample_theme_options[audio-url-2]" value="<?php esc_attr_e( $options['audio-url-2'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Background Image URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[image-url-2]" class="regular-text" type="text" name="sample_theme_options[image-url-2]" value="<?php esc_attr_e( $options['image-url-2'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Mobile Background Image URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[mobile-image-url-2]" class="regular-text" type="text" name="sample_theme_options[mobile-image-url-2]" value="<?php esc_attr_e( $options['mobile-image-url-2'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Video Content Header', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[video-content-header-2]" class="regular-text" type="text" name="sample_theme_options[video-content-header-2]" value="<?php esc_attr_e( $options['video-content-header-2'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Video Content', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[video-content-2]" class="regular-text" type="text" name="sample_theme_options[video-content-2]" value="<?php esc_attr_e( $options['video-content-2'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Video Button Name', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[video-button-2]" class="regular-text" type="text" name="sample_theme_options[video-button-2]" value="<?php esc_attr_e( $options['video-button-2'] ); ?>" />
					</td>
				</tr>
			</table>

			<table class="form-table" style="margin-left:30px;">
				<h3>3rd Video Options</h3>
				<tr valign="top"><th scope="row"><?php _e( 'Ambient Video URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[ambient-url-3]" class="regular-text" type="text" name="sample_theme_options[ambient-url-3]" value="<?php esc_attr_e( $options['ambient-url-3'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Audio Video URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[audio-url-3]" class="regular-text" type="text" name="sample_theme_options[audio-url-3]" value="<?php esc_attr_e( $options['audio-url-3'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Background Image URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[image-url-3]" class="regular-text" type="text" name="sample_theme_options[image-url-3]" value="<?php esc_attr_e( $options['image-url-3'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Mobile Background Image URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[mobile-image-url-3]" class="regular-text" type="text" name="sample_theme_options[mobile-image-url-3]" value="<?php esc_attr_e( $options['mobile-image-url-3'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Video Content Header', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[video-content-header-3]" class="regular-text" type="text" name="sample_theme_options[video-content-header-3]" value="<?php esc_attr_e( $options['video-content-header-3'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Video Content', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[video-content-3]" class="regular-text" type="text" name="sample_theme_options[video-content-3]" value="<?php esc_attr_e( $options['video-content-3'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Video Button Name', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[video-button-3]" class="regular-text" type="text" name="sample_theme_options[video-button-3]" value="<?php esc_attr_e( $options['video-button-3'] ); ?>" />
					</td>
				</tr>
			</table>

			<h2 style="margin-top:30px;">Mega menu options</h2>
			<hr/>
			<table class="form-table">
				<tr valign="top"><th scope="row"><?php _e( 'Appointment Dropdown Offer', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[appointment-offer]" class="regular-text" type="text" name="sample_theme_options[appointment-offer]" value="<?php esc_attr_e( $options['appointment-offer'] ); ?>" />
					</td>
				</tr>
			</table>
			<table class="form-table" style="margin-left:30px;">
				<h3>Furniture Section</h3>
				<tr valign="top"><th scope="row"><?php _e( 'Living Image URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[living-image]" class="regular-text" type="text" name="sample_theme_options[living-image]" value="<?php esc_attr_e( $options['living-image'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Dining Image URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[dining-image]" class="regular-text" type="text" name="sample_theme_options[dining-image]" value="<?php esc_attr_e( $options['dining-image'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Bed Image URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[bed-image]" class="regular-text" type="text" name="sample_theme_options[bed-image]" value="<?php esc_attr_e( $options['bed-image'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Office Image URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[office-image]" class="regular-text" type="text" name="sample_theme_options[office-image]" value="<?php esc_attr_e( $options['office-image'] ); ?>" />
					</td>
				</tr>
			</table>
			<table class="form-table" style="margin-left:30px;">
				<h3>Craftsmanship Section</h3>
				<tr valign="top"><th scope="row"><?php _e( 'Our Story Image URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[ourstory-image]" class="regular-text" type="text" name="sample_theme_options[ourstory-image]" value="<?php esc_attr_e( $options['ourstory-image'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'The Guys Image URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[theguys-image]" class="regular-text" type="text" name="sample_theme_options[theguys-image]" value="<?php esc_attr_e( $options['theguys-image'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Quality Image URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[quality-image]" class="regular-text" type="text" name="sample_theme_options[quality-image]" value="<?php esc_attr_e( $options['quality-image'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Reviews Image URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[reviews-image]" class="regular-text" type="text" name="sample_theme_options[reviews-image]" value="<?php esc_attr_e( $options['reviews-image'] ); ?>" />
					</td>
				</tr>
			</table>

			<h2 style="margin-top:30px;">Showroom Page Options</h2>
			<hr/>
			<table class="form-table">
				<tr valign="top"><th scope="row"><?php _e( 'Ambient Video URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[ambient-url-showroom]" class="regular-text" type="text" name="sample_theme_options[ambient-url-showroom]" value="<?php esc_attr_e( $options['ambient-url-showroom'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Audio Video URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[audio-url-showroom]" class="regular-text" type="text" name="sample_theme_options[audio-url-showroom]" value="<?php esc_attr_e( $options['audio-url-showroom'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Offer 1 Content', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[offer-1-content]" class="regular-text" type="text" name="sample_theme_options[offer-1-content]" value="<?php esc_attr_e( $options['offer-1-content'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Offer 1 Button Text', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[offer-1-text]" class="regular-text" type="text" name="sample_theme_options[offer-1-text]" value="<?php esc_attr_e( $options['offer-1-text'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Offer 1 URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[offer-1-url]" class="regular-text" type="text" name="sample_theme_options[offer-1-url]" value="<?php esc_attr_e( $options['offer-1-url'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Offer 2 Content', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[offer-2-content]" class="regular-text" type="text" name="sample_theme_options[offer-2-content]" value="<?php esc_attr_e( $options['offer-2-content'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Offer 2 Text', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[offer-2-text]" class="regular-text" type="text" name="sample_theme_options[offer-2-text]" value="<?php esc_attr_e( $options['offer-2-text'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Offer 2 URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[offer-2-url]" class="regular-text" type="text" name="sample_theme_options[offer-2-url]" value="<?php esc_attr_e( $options['offer-2-url'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Offer 3 Content', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[offer-3-content]" class="regular-text" type="text" name="sample_theme_options[offer-3-content]" value="<?php esc_attr_e( $options['offer-3-content'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Offer 3 Text', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[offer-3-text]" class="regular-text" type="text" name="sample_theme_options[offer-3-text]" value="<?php esc_attr_e( $options['offer-3-text'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Offer 3 URL', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[offer-3-url]" class="regular-text" type="text" name="sample_theme_options[offer-3-url]" value="<?php esc_attr_e( $options['offer-3-url'] ); ?>" />
					</td>
				</tr>
			</table>

			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e( 'Save Options', 'sampletheme' ); ?>" />
			</p>
		</form>
	</div>
	<?php
}

/**
 * Sanitize and validate input. Accepts an array, return a sanitized array.
 */
function theme_options_validate( $input ) {
	global $select_options, $radio_options;

	// Say our text option must be safe text with no HTML tags
	$input['ambient-url-1'] = wp_filter_nohtml_kses( $input['ambient-url-1'] );
	$input['audio-url-1'] = wp_filter_nohtml_kses( $input['audio-url-1'] );
	$input['image-url-1'] = wp_filter_nohtml_kses( $input['image-url-1'] );
	$input['mobile-image-url-1'] = wp_filter_nohtml_kses( $input['mobile-image-url-1'] );
	$input['video-content-header-1'] = wp_filter_nohtml_kses( $input['video-content-header-1'] );
	$input['video-content-1'] = wp_filter_nohtml_kses( $input['video-content-1'] );
	$input['video-button-1'] = wp_filter_nohtml_kses( $input['video-button-1'] );
	$input['ambient-url-2'] = wp_filter_nohtml_kses( $input['ambient-url-2'] );
	$input['audio-url-2'] = wp_filter_nohtml_kses( $input['audio-url-2'] );
	$input['image-url-2'] = wp_filter_nohtml_kses( $input['image-url-2'] );
	$input['mobile-image-url-2'] = wp_filter_nohtml_kses( $input['mobile-image-url-2'] );
	$input['video-content-header-2'] = wp_filter_nohtml_kses( $input['video-content-header-2'] );
	$input['video-content-2'] = wp_filter_nohtml_kses( $input['video-content-2'] );
	$input['video-button-2'] = wp_filter_nohtml_kses( $input['video-button-2'] );
	$input['ambient-url-3'] = wp_filter_nohtml_kses( $input['ambient-url-3'] );
	$input['audio-url-3'] = wp_filter_nohtml_kses( $input['audio-url-3'] );
	$input['image-url-3'] = wp_filter_nohtml_kses( $input['image-url-3'] );
	$input['mobile-image-url-3'] = wp_filter_nohtml_kses( $input['mobile-image-url-3'] );
	$input['video-content-header-3'] = wp_filter_nohtml_kses( $input['video-content-header-3'] );
	$input['video-content-3'] = wp_filter_nohtml_kses( $input['video-content-3'] );
	$input['video-button-3'] = wp_filter_nohtml_kses( $input['video-button-3'] );
	$input['living-image'] = wp_filter_nohtml_kses( $input['living-image'] );
	$input['dining-image'] = wp_filter_nohtml_kses( $input['dining-image'] );
	$input['bed-image'] = wp_filter_nohtml_kses( $input['bed-image'] );
	$input['office-image'] = wp_filter_nohtml_kses( $input['office-image'] );
	$input['ourstory-image'] = wp_filter_nohtml_kses( $input['ourstory-image'] );
	$input['theguys-image'] = wp_filter_nohtml_kses( $input['theguys-image'] );
	$input['quality-image'] = wp_filter_nohtml_kses( $input['quality-image'] );
	$input['reviews-image'] = wp_filter_nohtml_kses( $input['reviews-image'] );
	$input['appointment-offer'] = wp_filter_nohtml_kses( $input['appointment-offer'] );
	$input['ambient-url-showroom'] = wp_filter_nohtml_kses( $input['ambient-url-showroom'] );
	$input['audio-url-showroom'] = wp_filter_nohtml_kses( $input['audio-url-showroom'] );
	$input['offer-1-content'] = wp_filter_nohtml_kses( $input['offer-1-content'] );
	$input['offer-1-text'] = wp_filter_nohtml_kses( $input['offer-1-text'] );
	$input['offer-1-url'] = wp_filter_nohtml_kses( $input['offer-1-url'] );
	$input['offer-2-content'] = wp_filter_nohtml_kses( $input['offer-2-content'] );
	$input['offer-2-text'] = wp_filter_nohtml_kses( $input['offer-2-text'] );
	$input['offer-2-url'] = wp_filter_nohtml_kses( $input['offer-2-url'] );
	$input['offer-3-content'] = wp_filter_nohtml_kses( $input['offer-3-content'] );
	$input['offer-3-text'] = wp_filter_nohtml_kses( $input['offer-3-text'] );
	$input['offer-3-url'] = wp_filter_nohtml_kses( $input['offer-3-url'] );

	return $input;
}